# From Masked-Language Modeling to Translation: Non-English Auxiliary Tasks Improve Zero-shot Spoken Language Understanding

This repository has been moved to [https://github.com/mainlp/xsid](https://github.com/mainlp/xsid)

